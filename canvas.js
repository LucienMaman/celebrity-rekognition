function canvas() {

    console.log('Canvas !');

    var canvas, context, xStart, yStart, xEnd, yEnd;

    canvas = document.getElementById("canvas");
    context = canvas.getContext("2d");
    canvas.addEventListener("mousedown", mouseDown);
    canvas.addEventListener("mouseup", mouseUp);

    function mouseDown(e) {
        xStart = e.offsetX;
        yStart = e.offsetY;
    }

    function mouseUp(e) {
        xEnd = e.offsetX;
        yEnd = e.offsetY;
        if (xStart != xEnd && yStart != yEnd) {
            var video = document.createElement("video");
            video.src = "tsonga.mp4";
            video.addEventListener('loadeddata', function () {
                video.play();
                context.drawImage(video, xStart, yStart, xEnd - xStart, yEnd - yStart);
            });
        }
    }
}

module.exports = {
    canvas
}