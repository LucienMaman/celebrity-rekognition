const express = require('express')
const app = express()
const canvas = require('./canvas.js');
let fs = require('fs');

app.set('view engine', 'ejs');
app.use(express.static('public'))

app.get('/', (req, res) => res.render('index'));

app.listen(3000, () => console.log('Example app listening on port 3000!'))


