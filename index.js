var AWS = require('aws-sdk');
let fs = require('fs');

AWS.config.credentials = new AWS.TemporaryCredentials({
  RoleArn: `arn:aws:iam::630843564847:role/super_admin`
})

var rekognition = new AWS.Rekognition({
  apiVersion: "2016-06-27",
  region: 'eu-west-1'
});

var params = {
  Video: {
    S3Object: {
      Bucket: 'rekognition-video-demo-dub-grabyo-dev',
      Name: 'ibTTjD0e1gC_flexar-720p-20150820_sbr.mp4',
    }
  },
  NotificationChannel: {
    RoleArn: 'arn:aws:iam::630843564847:role/super_admin',
    SNSTopicArn: 'arn:aws:sns:eu-west-1:630843564847:ssssssss'
  }
};

//To test with image
const image = fs.readFileSync('tsonga.png');

const murrayImage = {
 Image: {
   Bytes: new Buffer(image),
 }
};

rekognition.recognizeCelebrities(murrayImage, function (err, data) {
  if (err) console.log(err, err.stack); // an error occurred
  else{
    console.log('\nrecognizeCelebrities--->', data.CelebrityFaces[0].Name); // successful response
    return data.CelebrityFaces[0].Name
  }
});
